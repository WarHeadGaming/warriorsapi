/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.xstream;

/**
 *
 * @author Somners
 */
public class XMana {

    public Integer base;
    public Integer max;
    public Integer skillPointIncrement;
}
