/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.xstream;

/**
 *
 * @author Somners
 */
public class XItem {

    public String name;
    public XRequirements requirements;
    public Integer priority;
    public Integer baseDamage;
    public Integer maxDamage;
    public Integer damageIncrement;
    public Integer baseAttackSpeed;
    public Integer minAttackSpeed;
    public Integer attackSpeedIncrement;

}
