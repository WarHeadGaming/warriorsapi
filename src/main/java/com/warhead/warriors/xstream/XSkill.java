/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.xstream;

/**
 *
 * @author Somners
 */
public class XSkill {

    public String name;
    public Integer priority;
    public Integer baseCastTime;
    public Integer minCastTime;
    public Integer castTimeIncrement;
    public Integer baseCoolDown;
    public Integer minCoolDown;
    public Integer coolDownIncrement;
    public Integer manaCost;
    public Integer staminaCost;
    public XRequirements requirements;
}
