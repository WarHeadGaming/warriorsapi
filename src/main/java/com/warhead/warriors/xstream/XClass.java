/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.xstream;

import java.util.List;

/**
 *
 * @author Somners
 */
public class XClass extends XAttributes {


    public XRequirements requirements;
    public List<String> bannedRaces;
    public List<String> bannedClasses;
}
