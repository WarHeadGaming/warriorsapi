/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys.filter;

import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.commandsys.CommandManager;
import com.warhead.warriors.commandsys.WarriorsCommand;
import org.spout.api.command.Command;
import org.spout.api.command.CommandArguments;
import org.spout.api.command.CommandSource;
import org.spout.api.command.filter.CommandFilter;
import org.spout.api.entity.Player;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Somners
 */
public class VerifyArgumentsFilter implements CommandFilter {

    @Override
    public void validate(Command cmnd, CommandSource cs, CommandArguments ca) throws CommandException {
        WarriorsCommand warcmd = CommandManager.get().getCommand(cmnd.getName());
        if (cs instanceof Player && warcmd.getCommandRequirements() != null && !warcmd.getCommandRequirements().verify(ca)) {
            cs.sendMessage(Warriors.get().getHeader() + "Improper Syntax: " + warcmd.getDescription());
            throw new CommandException(cs.getName() + " used the improper syntax for command " + warcmd.getCommand().getName());
        }

    }

}
