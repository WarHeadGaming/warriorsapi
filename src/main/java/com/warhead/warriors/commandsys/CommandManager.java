/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys;

import com.warhead.warriors.commandsys.commands.*;
import com.warhead.warriors.commandsys.filter.VerifyArgumentsFilter;
import java.util.HashMap;
import org.spout.api.command.filter.CommandFilter;

/**
 *
 * @author Somners
 */
public class CommandManager {

    private static CommandManager instance = null;
    private WarriorsCommand root;
    private HashMap<String, WarriorsCommand> commands = new HashMap<String, WarriorsCommand>();
    private CommandFilter verifyFilter = new VerifyArgumentsFilter();

    public CommandManager() {
        /*
         * initialise root command
         */
        root = new RootCommand();
        register(root);
        /*
         * initialise other commands
         */
        new CreateCommand();
    }

    /**
     * Gets or initializes the CommandManager instance.
     * @return CommandManager
     */
    public static CommandManager get() {
        if (instance == null) {
            instance = new CommandManager();
        }
        return instance;
    }

    /**
     * Stores this command within the manager for later retreival.
     * @param command The command to register.
     */
    public void register(WarriorsCommand command) {
        if (!commands.containsKey(command.getCommand().getName())) {
            commands.put(command.getCommand().getName(), command);
        }
    }

    /**
     * Gets a WarriorsCommand by the given name.
     * @param name The name of the Command to get.
     * @return The { @link WarrorsCommand } instance or null if it doesn't exist.
     */
    public WarriorsCommand getCommand(String name) {
        return this.commands.get(name);
    }

    public WarriorsCommand getRoot() {
        return root;
    }

    public CommandFilter getVerifyArgumentsFilter() {
        return this.verifyFilter;
    }

}
