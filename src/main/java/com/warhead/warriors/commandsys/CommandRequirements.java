/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.spout.api.command.CommandArguments;

/**
 *
 * @author Somners
 */
public class CommandRequirements {

    private List<String> required = null;
    private String prefix = "-";

    public CommandRequirements(String... required) {
        this.required = Arrays.asList(required);
    }

    public boolean verify(CommandArguments args) {
        List<String> copy = new ArrayList<String>(args.get());
        for (String s : required) {
            if (!checkFirst(copy, s) && !checkFull(copy, s)) {
                return false;
            }
        }
        return true;
    }

    public boolean verifyTest(List<String> args) {
        List<String> copy = new ArrayList<String>(args);
        for (String s : required) {
            if (!checkFirst(copy, s) && !checkFull(copy, s)) {
                return false;
            }
        }
        return true;
    }

    private boolean checkFirst(List<String> args, String arg) {
        for (String s : args) {
            if (s.contains(prefix)) {
                if (s.length() == prefix.length() + 1 && s.indexOf(prefix.length()) == arg.indexOf(0)) {
                    args.remove(s);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkFull(List<String> args, String arg) {
        for (String s : args) {
            if (s.contains(prefix)) {
                if (s.equalsIgnoreCase(prefix + arg)) {
                    args.remove(s);
                    return true;
                }
            }
        }
        return false;
    }

}
