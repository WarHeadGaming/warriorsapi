/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys.commands;

import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.api.achievements.Achievement;
import com.warhead.warriors.api.race.Race;
import com.warhead.warriors.api.rpgclass.RPGClass;
import com.warhead.warriors.commandsys.CommandHelper;
import com.warhead.warriors.commandsys.WarriorsCommand;
import com.warhead.warriors.commandsys.selection.CharacterSelection;
import org.spout.api.command.Command;
import org.spout.api.command.CommandArguments;
import org.spout.api.command.CommandSource;
import org.spout.api.entity.Player;
import org.spout.api.exception.CommandException;
import org.spout.vanilla.ChatStyle;

/**
 *
 * @author Somners
 */
public class CreateCommand extends WarriorsCommand {

    @Override
    public Command getCommand() {
        if (command == null) {
            this.name = "create";
            this.alias = array("choose");
            this.requirements = array("race", "class");
        }
        return super.getCommand();
    }

    @Override
    public void execute(CommandSource cs, Command cmnd, CommandArguments ca) throws CommandException {
        Race race = Warriors.getRaceLoader().get(CommandHelper.getStringArg(ca, "race"));
        RPGClass rpg = Warriors.getRPGClassLoader().get(CommandHelper.getStringArg(ca, "class"));
        if (race == null) {
           cs.sendMessage(String.format("%s Race '%s' was not found.", Warriors.getHeader(), CommandHelper.getStringArg(ca, "race")));
           return;
        }
        if (rpg == null) {
           cs.sendMessage(String.format("%s Class '%s' was not found.", Warriors.getHeader(), CommandHelper.getStringArg(ca, "class")));
           return;
        }
        if (race.getBannedRPGClasses().contains(rpg.getName())) {
           cs.sendMessage(String.format("%s Race '%s' cannot have Class of type '%s'. Please choose another.", Warriors.getHeader(), race.getName(), rpg.getName()));
           return;
        }

        if (rpg.getBannedRaces().contains(race.getName())) {
           cs.sendMessage(String.format("%s Class '%s' cannot have Race of type '%s'. Please choose another.", Warriors.getHeader(), rpg.getName(), race.getName()));
           return;
        }
        if (rpg.getRequirements().getLevel() > 0) {
           cs.sendMessage(String.format("%s Class '%s' requires a level of '%s'. Please choose another.", Warriors.getHeader(), rpg.getName(), rpg.getRequirements().getLevel()));
           return;
        }
        if (rpg.getRequirements().getRPGClass() != null) {
           cs.sendMessage(String.format("%s Class '%s' requires Class '%s'. Please choose another.", Warriors.getHeader(), rpg.getName(), rpg.getRequirements().getRPGClass().getName()));
           return;
        }
        if (!rpg.getRequirements().getRace().equals(race)) {
           cs.sendMessage(String.format("%s Class '%s' requires Race '%s'. Please choose another.", Warriors.getHeader(), rpg.getName(), rpg.getRequirements().getRace().getName()));
           return;
        }
        for (Achievement a: rpg.getRequirements().getAchievements()) {
            if (!a.passesRequirement((Player)cs)) {
                cs.sendMessage(String.format("%s Class '%s' requires Achievement '%s'. Please choose another.", Warriors.getHeader(), rpg.getName(), a.getName()));
                return;
            }
        }
        cs.sendMessage(String.format("%s Your current Character Selection:", Warriors.getHeader()));
        cs.sendMessage(String.format("%s - RPGClass:%s %s", ChatStyle.BLUE, ChatStyle.WHITE, rpg.getName()));
        cs.sendMessage(String.format("%s - Race:%s %s", ChatStyle.BLUE, ChatStyle.WHITE, race.getName()));
        cs.sendMessage(String.format("%s Please Type %s/accept%s to accept this selection. "
                + "If you already have a Class/Race it will be overwritten and reset. "
                + "This cannot be undone.", ChatStyle.RED, ChatStyle.WHITE, ChatStyle.RED));
        Warriors.getCommandSelections().addSelection(new CharacterSelection((Player)cs, race, rpg));
    }


}
