/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys.commands;

import com.warhead.warriors.commandsys.WarriorsCommand;
import org.spout.api.command.Command;
import org.spout.api.command.CommandArguments;
import org.spout.api.command.CommandSource;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Somners
 */
public class RootCommand extends WarriorsCommand {

    @Override
    public Command getCommand() {
        this.name = "warriors";
        this.alias = array("warrior", "w");
        this.permission = "warriors.commands.root";
        return super.getCommand();
    }

    @Override
    public void execute(CommandSource cs, Command cmnd, CommandArguments ca) throws CommandException {

    }

}
