/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys.commands;

import com.warhead.warriors.commandsys.WarriorsCommand;
import org.spout.api.command.Command;
import org.spout.api.command.CommandArguments;
import org.spout.api.command.CommandSource;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Somners
 */
public class AcceptCommand extends WarriorsCommand {

    @Override
    public Command getCommand() {
        if (this.command != null) {
            this.name = "accept"
        }
        return super.getCommand();
    }

    @Override
    public void execute(CommandSource source, Command command, CommandArguments args) throws CommandException {
        throw new UnsupportedOperationException("Method 'execute' in class 'AcceptCommand' is not supported yet.");
    }

}
