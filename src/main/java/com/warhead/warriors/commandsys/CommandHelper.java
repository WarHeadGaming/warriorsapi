/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys;

import java.util.List;
import org.spout.api.command.CommandArguments;

/**
 *
 * @author Somners
 */
public class CommandHelper {

    public static String prefix = "-";

    public static String getStringArg(CommandArguments args, String arg) {
        StringBuilder sb = new StringBuilder();
        boolean append = false;
        for (String s : args.get()) {
            if (append && s.contains(prefix)) {
                append = false;
            }
            else if (s.equalsIgnoreCase(prefix + arg) || s.equalsIgnoreCase(prefix + arg.charAt(0))) {
                append = true;
            }
            else if (append) {
                sb.append(s).append(" ");
            }
        }
        return sb.toString().trim();
    }

    public static Double getDoubleArg(CommandArguments args, String arg) {
        return Double.valueOf(CommandHelper.getStringArg(args, arg));
    }

    public static Integer getIntArg(CommandArguments args, String arg) {
        return Integer.valueOf(CommandHelper.getStringArg(args, arg));
    }

    public static Float getFloatArg(CommandArguments args, String arg) {
        return Float.valueOf(CommandHelper.getStringArg(args, arg));
    }

    public static boolean verifyArgs(CommandArguments params, CommandRequirements required) {
        return required.verify(params);
    }

  public static String getStringArg(List<String> args, String arg) {
        StringBuilder sb = new StringBuilder();
        boolean append = false;
        for (String s : args) {
            if (append && s.contains(prefix)) {
                append = false;
            }
            else if (s.equalsIgnoreCase(prefix + arg)) {
                append = true;
            }
            else if (append) {
                sb.append(s).append(" ");
            }
        }
        return sb.toString().trim();
    }
}
