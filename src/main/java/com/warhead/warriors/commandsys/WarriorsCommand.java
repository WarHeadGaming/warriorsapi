/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys;

import org.spout.api.Spout;
import org.spout.api.command.Command;
import org.spout.api.command.Executor;

/**
 *
 * @author Somners
*/
public abstract class WarriorsCommand implements Executor {

    /**
     * Instance of this class.
     */
    protected WarriorsCommand instance = null;
    /**
     * Spout { @link Command } that this class handles.
     */
    protected Command command = null;
    /**
     * Name of the command, a.k.a. the base of the command used in game.
     */
    protected String name = null;
    /**
     * Aliases for this command that can be used alternatively to the name.
     */
    protected String[] alias = null;
    /**
     * The Permission for this Command.
     */
    protected String permission = null;
    /**
     * String array of required permissions.
     */
    protected String[] requirements = null;
    /**
     * Requirements Comparator for use by the { @link VerifyArgumentsFilter }.
     */
    protected CommandRequirements requirement = null;
    /**
     * The command line or chat usage for this command
     */
    protected String usage = "";
    /**
     * Description of what this command does.
     */
    protected String description = "";


    public WarriorsCommand() {
        this.instance = this;
        this.getCommand();
        CommandManager.get().register(instance);
    }

    /**
     * Gets or initializes the command and returns the { @link Command }.
     * @return The { @link Command }.
     */
    public Command getCommand() {
        if (command == null) {
            command = Spout.getCommandManager().getCommand(name);
            if (permission != null) {
                command.setPermission(permission);
            }
            if (alias != null) {
                command.addAlias(alias);
            }
            if (requirements != null) {
                requirement = new CommandRequirements(requirements);
                command.addFilter(CommandManager.get().getVerifyArgumentsFilter());
            }

        }

        return command;
    }

    /**
     * Gets the { @link CommandRequirements } for this Command
     * @return The { @link CommandRequirements } or null if it doesn't have any
     *          parameter requirements.
     */
    public CommandRequirements getCommandRequirements() {
        return this.requirement;
    }

    /**
     * Gets the description of what this command does.
     * @return The Description.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Gets the console or chat usage of this command.
     * @return The Usage.
     */
    public String getUsage() {
        return this.usage;
    }

    /**
     * Helper method for creating a String array.
     * @param args Arguments for this array.
     * @return A String[]
     */
    protected String[] array(String... args) {
        return args;
    }

}
