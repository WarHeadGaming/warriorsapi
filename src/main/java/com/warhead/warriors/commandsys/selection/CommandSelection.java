/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys.selection;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somners
 */
public class CommandSelection {

    private List<Selection> list = new ArrayList<Selection>();

    /**
     * Gets the players Selection
     * @param player Player name to get the latest selection for.
     * @return The players selection.
     */
    public Selection getSelection(String player) {
        for (Selection s : list) {
            if (s.getPlayer().equals(player)) {
                return s;
            }
        }
        return null;
    }

    /**
     * Adds the given selection to the list and removes any prior selections
     * made by this player.
     * @param selection The selection to add.
     */
    public void addSelection(Selection selection) {
        for (Selection s : list) {
            if (s.getPlayer().equals(selection.getPlayer())) {
                list.remove(s);
            }
        }
        list.add(selection);
    }

    /**
     * Removes all Selections from the given player name, though there should
     * never be more than one.
     * @param player The player name to remove all Selections for.
     */
    public void removeSelection(String player) {
        for (Selection s : list) {
            if (s.getPlayer().equals(player)) {
                list.remove(s);
            }
        }
    }

}
