/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys.selection;

/**
 *
 * @author Somners
 */
public interface Selection {

    /**
     * Gets the player name this Selection is for.
     * @return
     */
    public String getPlayer();
}
