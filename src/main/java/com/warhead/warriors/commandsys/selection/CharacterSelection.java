/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys.selection;

import com.warhead.warriors.api.race.Race;
import com.warhead.warriors.api.rpgclass.RPGClass;
import org.spout.api.entity.Player;

/**
 *
 * @author Somners
 */
public class CharacterSelection implements Selection {

    private Player player;
    private Race race;
    private RPGClass rpg;

    public CharacterSelection(Player player, Race race, RPGClass rpg) {
        this.player = player;
        this.race = race;
        this.rpg = rpg;
    }

    @Override
    public String getPlayer() {
        return player.getName();
    }

    public Race getRace() {
        return race;
    }

    public RPGClass getRPGClass() {
        return rpg;
    }
}
