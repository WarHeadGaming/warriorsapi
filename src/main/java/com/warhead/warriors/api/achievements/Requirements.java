/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.achievements;

import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.race.Race;
import com.warhead.warriors.api.rpgclass.RPGClass;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somners
 */
public class Requirements {

    private Integer level = null;
    private List<Achievement> achievements = new ArrayList<Achievement>();
    private RPGClass rpgclass = null;
    private Race race = null;

    public Requirements(Integer level, List<String> achievements, String rpgclass, String race) {
        this.level = level;
        for (String s : achievements) {
            Achievement a = Warriors.getAchievmentLoader().get(s);
            if (a == null) {
                Warriors.get().logger().warning(String.format("Tried to use an unloaded achievement: %s", s));
                continue;
            }
            this.achievements.add(a);
        }
        this.rpgclass = rpgclass == null ? null : Warriors.getRPGClassLoader().get(rpgclass);
        if (rpgclass != null && this.rpgclass == null) {
            Warriors.get().logger().warning(String.format("Tried to use an unloaded RPG Class: %s", rpgclass));
        }
    }

    /**
     * Checks if this player meets the requirements defined by this class instance.
     * @param player RPGComponent belonging to the player wishing to be checked.
     * @return
     */
    public boolean meetsRequirements(RPGComponent player) {
        if (!(player.getExperience().getLevel() >= this.getLevel())) {
            return false;
        }
        if (this.rpgclass != null && !player.getRPGClasses().contains(this.rpgclass)) {
            return false;
        }
        if (this.race != null && !player.getRace().equals(this.race)) {
            return false;
        }
        for (Achievement a : this.getAchievements()) {
            if (!a.passesRequirement(player.getSpoutPlayer())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets the required Level for this requirement
     * @return
     */
    public int getLevel() {
        return level == null ? 0 : level;
    }

    /**
     * Gets the required Achievements for this Requirement
     * @return
     */
    public List<Achievement> getAchievements() {
        return achievements;
    }

    /**
     * Gets the required { @link Race} for this Requirement
     * @return
     */
    public Race getRace() {
        return this.race;
    }

    /**
     * Gets the required { @link RPGClass } for this Requirement
     * @return
     */
    public RPGClass getRPGClass() {
        return this.rpgclass;
    }
}
