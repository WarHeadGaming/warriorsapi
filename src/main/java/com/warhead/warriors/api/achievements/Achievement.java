/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.achievements;

import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.event.Triggerable;
import org.spout.api.entity.Player;

/**
 *
 * @author Somners
 */
public interface Achievement extends Triggerable, Named {
    /**
     * The name of this achievement.
     * @return The name.
     */
    public String getName();

    /**
     * Called when the Achievement is loaded.
     */
    public void onLoad();

    /**
     * Checks if the player has achieved this achievement.
     * @param player The player to check for.
     * @return true if the player has passed the achievement, false otherwise.
     */
    public boolean passesRequirement(Player player);
}
