/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api;

import com.warhead.warriors.api.item.ItemEntry;
import com.warhead.warriors.api.achievements.Requirements;
import com.warhead.warriors.api.player.attributes.Health;
import org.spout.api.entity.Player;

/**
 *
 * @author Somners
 */
public interface Factory {

    /**
     * Gets the { @link ItemEntry } by the given name.  This method is to check
     * if the item exists in the implemented game.
     * @param name Name of the Item to check.
     * @return { @link ItemEntry } if exists, null if it does not.
     */
    public ItemEntry getItem(String item, int priority, int baseAttackSpeed, int minAttackSpeed, int attackSpeedIncrement,
            int baseDamage, int maxDamage, int damageIncrement, Requirements requirements);

    /**
     * Gets the { @link Health } for the given player.  This method is to create
     * a Health wrapper for the implemented game.
     * @param player Player to get a { @link Health } instance for.
     * @return { @link Health }
     */
    public Health getHealth(Player player);
}
