/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.attributes.data.RPGPlayerAccess;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.canarymod.database.Database;
import net.canarymod.database.exceptions.DatabaseWriteException;

/**
 *
 * @author Somners
 */
public class Stamina implements SkillPointMaxable {

    private RPGPlayerAccess access;
    private RPGComponent player;

    public Stamina(RPGPlayerAccess access, RPGComponent player) {
        this.access = access;
        this.player = player;
    }

    /**
     * Gets this player's Stamina.
     * @return The player's Stamina.
     */
    public int getStamina() {
        return access.stamina;
    }

    /**
     * Sets this player's Stamina to the given value.
     * @param toSet The value to set the Stamina to.
     */
    public void setStamina(int toSet) {
        this.access.stamina = toSet;
        try {
            Database.get().update(access, new String[]{"stamina"}, new Object[]{this.access.stamina});
        } catch (DatabaseWriteException ex) {
            Logger.getLogger(Stamina.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Adds the given value to the player's Stamina.
     * @param toAdd The amount to add to the player's Stamina.
     */
    public void addStamina(int toAdd) {
        this.setStamina(this.access.stamina + toAdd);
    }

    /**
     * Removes the given value from the player's Stamina.
     * @param toRemove The amount to remove from the player's Stamina.
     */
    public void removeStamina(int toRemove) {
        this.setStamina(this.access.stamina - toRemove);
    }

    @Override
    public boolean isSkillPointMaxed() {
        return (this.player.getRace().getMaxStamina() - this.player.getRace().getBaseStamina()) <=
                (player.getRace().getStaminaIncrement() * this.player.getSkillPoints().getPoints(SkillPointType.STAMINA));
    }
}
