/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.attributes.data.RPGPlayerAccess;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.canarymod.database.Database;
import net.canarymod.database.exceptions.DatabaseWriteException;

/**
 *
 * @author Somners
 */
public class Mana implements SkillPointMaxable {

    private RPGComponent player;
    private RPGPlayerAccess access;

    public Mana(RPGPlayerAccess access, RPGComponent player) {
        this.access = access;
        this.player = player;
    }

    /**
     * Gets this player's Mana.
     * @return The player's Mana.
     */
    public int getMana() {
        return this.access.mana;
    }

    /**
     * Sets this player's Mana to the given value.
     * @param toSet The value to set the Mana to.
     */
    public void setMana(int toSet) {
        this.access.mana = toSet;
        try {
            Database.get().update(access, new String[]{"mana"}, new Object[]{this.access.mana});
        } catch (DatabaseWriteException ex) {
            Logger.getLogger(Mana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Adds the given value to the player's Mana.
     * @param toAdd The amount to add to the player's Mana.
     */
    public void addMana(int toAdd) {
        this.setMana(this.access.mana + toAdd);
    }

    /**
     * Removes the given value from the player's Mana.
     * @param toRemove The amount to remove from the player's Mana.
     */
    public void removeMana(int toRemove) {
        this.setMana(this.access.mana - toRemove);
    }

    @Override
    public boolean isSkillPointMaxed() {
        return (this.player.getRace().getMaxMana() - this.player.getRace().getBaseMana()) <=
                (player.getRace().getManaIncrement() * this.player.getSkillPoints().getPoints(SkillPointType.MANA));
    }
}
