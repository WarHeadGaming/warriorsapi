/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

/**
 *
 * @author Somners
 */
public interface Health extends SkillPointMaxable {

    /**
     * Gets this player's Health.
     * @return The player's Health.
     */
    public int getHealth();

    /**
     * Sets this player's Health to the given value.
     * @param toSet The value to set the Health to.
     */
    public void setHealth(int toSet);

    /**
     * Adds the given value to the player's Health.
     * @param toAdd The amount to add to the player's Health.
     */
    public void addHealth(int toAdd);

    /**
     * Removes the given value from the player's Health.
     * @param toRemove The amount to remove from the player's Health.
     */
    public void removeHealth(int toRemove);
}
