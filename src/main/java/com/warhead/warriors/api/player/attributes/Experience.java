/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.attributes.data.RPGPlayerAccess;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.canarymod.database.Database;
import net.canarymod.database.exceptions.DatabaseWriteException;

/**
 *
 * @author Somners
 */
public class Experience {


    private RPGComponent player;
    private RPGPlayerAccess access;

    public Experience(RPGPlayerAccess access, RPGComponent player) {
        this.access = access;
        this.player = player;
    }

    /**
     * Gets the { @link RPGComponent }'s level<br>
     * <b>Note: </b> For testin purposes it is xp/10
     * @return Level.
     */
    public int getLevel() {
        return (int) this.access.xp / 10;
    }

    /**
     * Gets this player's XP.
     * @return The player's XP.
     */
    public int getXP() {
        return this.access.xp;
    }

    /**
     * Sets this player's XP to the given value.
     * @param toSet The value to set the XP to.
     */
    public void setXP(int toSet) {
        this.access.xp = toSet;
        try {
            Database.get().update(access, new String[]{"xp"}, new Object[]{this.access.xp});
        } catch (DatabaseWriteException ex) {
            Logger.getLogger(Experience.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Adds the given value to the player's XP.
     * @param toAdd The amount to add to the player's XP.
     */
    public void addXP(int toAdd) {
        this.setXP(this.access.xp + toAdd);
    }

    /**
     * Removes the given value from the player's XP.
     * @param toRemove The amount to remove from the player's XP.
     */
    public void removeXP(int toRemove) {
        this.setXP(this.access.xp - toRemove);
    }
}
