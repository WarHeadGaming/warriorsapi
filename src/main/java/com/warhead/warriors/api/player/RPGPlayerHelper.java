/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player;

import com.warhead.warriors.api.item.ItemEntry;
import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.player.attributes.Attributes;
import com.warhead.warriors.api.skills.SkillEntry;
import java.util.ArrayList;
import java.util.List;
import org.spout.api.entity.Player;

/**
 *
 * @author Somners
 */
public class RPGPlayerHelper {

    private static List<RPGComponent> players = new ArrayList<RPGComponent>();

    /**
     * Gets the RPGComponent instance of the given spout player.
     * @param player Spout player to get for.
     * @return RPGComponent or null if there is none.
     */
    public static RPGComponent getPlayer(Player player) {
        for (RPGComponent p : players) {
            if (p.getSpoutPlayer().equals(player)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Registers the given RPGComponent with the Warriors tracker. It checks to see
     * if there is already a registered RPGComponent for the given Spout Player.
     * @param player The RPGComponent to track.
     */
    public static void registerPlayer(RPGComponent player) {
        if (getPlayer(player.getSpoutPlayer()) != null) {
            return;
        }
        players.add(player);
    }

    /**
     * Gets the { @link SkillEntry } from the player that has the highest priority.
     * @param player Player to get the { @link SkillEntry } from.
     * @param skillName Name of the { @link SkillEntry }.
     * @return The { @link SkillEntry } with the Highest Priority
     */
    public static SkillEntry getPrioritySkill(RPGComponent player, String skillName) {
        List<SkillEntry> skills = new ArrayList<SkillEntry>();
        for (Attributes a : player.getAttributes()) {
            if (a.canUseSkill(skillName, player)) {
                skills.add(a.getSkill(skillName));
            }
        }
        SkillEntry toRet = null;
        for (SkillEntry s : skills) {
            if (toRet == null) {
                toRet = s;
                continue;
            }
            if (s.getPriority() > toRet.getPriority()) {
                toRet = s;
            }
        }
        return toRet;
    }

    /**
     * Gets the { @link ItemEntry } from the player that has the highest priority.
     * @param player Player to get the { @link ItemEntry } from.
     * @param itemName Name of the { @link ItemEntry }.
     * @return The { @link ItemEntry } with the Highest Priority
     */
    public static ItemEntry getPriorityItem(RPGComponent player, String itemName) {
        List<ItemEntry> items = new ArrayList<ItemEntry>();
        for (Attributes a : player.getAttributes()) {
            if (a.canUseItem(itemName, player)) {
                items.add(a.getItem(itemName));
            }
        }
        ItemEntry toRet = null;
        for (ItemEntry s : items) {
            if (toRet == null) {
                toRet = s;
                continue;
            }
            if (s.getPriority() > toRet.getPriority()) {
                toRet = s;
            }
        }
        return toRet;
    }
}
