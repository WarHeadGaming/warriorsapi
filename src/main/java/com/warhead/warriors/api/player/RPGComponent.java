/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player;

import com.warhead.warriors.api.item.ItemEntry;
import com.warhead.warriors.api.Saveable;
import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.api.achievements.Achievement;
import com.warhead.warriors.api.attributes.data.RPGPlayerAccess;
import com.warhead.warriors.api.attributes.data.SkillPointAccess;
import com.warhead.warriors.api.player.attributes.Attributes;
import com.warhead.warriors.api.player.attributes.Experience;
import com.warhead.warriors.api.player.attributes.Health;
import com.warhead.warriors.api.player.attributes.Mana;
import com.warhead.warriors.api.player.attributes.SkillPoints;
import com.warhead.warriors.api.player.attributes.Stamina;
import com.warhead.warriors.api.race.Race;
import com.warhead.warriors.api.rpgclass.RPGClass;
import com.warhead.warriors.api.skills.SkillEntry;
import java.util.ArrayList;
import java.util.List;
import net.canarymod.database.DataAccess;
import net.canarymod.database.Database;
import net.canarymod.database.exceptions.DatabaseReadException;
import net.canarymod.database.exceptions.DatabaseWriteException;
import org.spout.api.component.entity.EntityComponent;
import org.spout.api.entity.Player;

/**
 *
 * @author Somners
 */
public abstract class RPGComponent extends EntityComponent implements Attributes, Saveable {

    private RPGPlayerAccess access = null;
    private Mana mana = null;
    private Stamina stamina = null;
    private Health health = null;
    private Experience experience = null;
    private SkillPoints points = null;
    private Player player = null;
    private List<RPGClass> classes = new ArrayList<RPGClass>();
    private Race race = null;

    /**
     * Gets the name of this player, convenience method for Spout Player name.
     * @return The name.
     */
    @Override
    public String getName() {
        return this.player.getName();
    }

    /**
     * Gets the spout player object for this player.
     * @return The player.
     */
    public Player getSpoutPlayer() {
        return this.player;
    }

    /**
     * Gets a list of all the Achievements this player has completed.
     * @return A list of Achievements.
     */
    public List<Achievement> getAcheivements() {
        List<Achievement> list = new ArrayList<Achievement>();
        for (Achievement a : Warriors.getAchievmentLoader().getAll()) {
            if (a.passesRequirement(player)) {
                list.add(a);
            }
        }
        return list;
    }

    /**
     * Gets a list of all the Achievement names this player has completed.
     * @return A list of Achievement names.
     */
    public List<String> getAcheivementNames() {
        List<String> list = new ArrayList<String>();
        for (Achievement a : Warriors.getAchievmentLoader().getAll()) {
            if (a.passesRequirement(player)) {
                list.add(a.getName());
            }
        }
        return list;

    }

    /**
     * Gets a list of this players { @link Attributes }.  { @link Attributes } are
     * a common interface between { @link RPGClass } and { @link Race } for common
     * attributes.
     * @return A list of { @link Attributes }.
     */
    public List<Attributes> getAttributes() {
        List<Attributes> list = new ArrayList<Attributes>();
        list.addAll(classes);
        list.add(race);
        return list;
    }

    /**
     * Gets this players { @link Race }.
     * @return A { @link Race }.
     */
    public Race getRace() {
        return this.race;
    }

    /**
     * Gets a list of this players { @link RPGClass }'s.
     * @return A list of { @link RPGClass }'s.
     */
    public List<RPGClass> getRPGClasses() {
        return this.classes;
    }

    /**
     * Gets a list of this players { @link RPGClass } names.
     * @return A list of { @link RPGClass } names.
     */
    public List<String> getRPGClassNames() {
        List<String> list = new ArrayList<String>();
        for (RPGClass c : classes) {
            list.add(c.getName());
        }
        return list;
    }

    /**
     * Gets this Player's instance of { @link SkillPoints }.
     * @return { @link SkillPoints }
     */
    public SkillPoints getSkillPoints() {
        return this.points;
    }

    /**
     * Gets the { @link Stamina } for this player.
     * @return { @link Stamina }
     */
    public Stamina getStamina() {
        return this.stamina;
    }

    /**
     * Gets the { @link Mana } for this player.
     * @return { @link Mana }
     */
    public Mana getMana() {
        return this.mana;
    }

    /**
     * Gets the { @link Health } for this player.
     * @return { @link Health }
     */
    public Health getHealth() {
        return this.health;
    }

    /**
     * Gets this players { @link Experience } instance.
     * @return This players { @link Experience } instance.
     */
    public Experience getExperience() {
        return this.experience;
    }

    @Override
    public boolean canUseSkill(String skillName, RPGComponent player) {
        boolean bool = false;
        for (Attributes a : this.getAttributes()) {
            if (a.canUseSkill(skillName, this)) {
                bool = true;
            }
        }
        return bool;
    }

    @Override
    public List<String> getSkillNames() {
        List<String> list = new ArrayList<String>();
        for (Attributes a : this.getAttributes()) {
            list.addAll(a.getSkillNames());
        }
        return list;
    }

    @Override
    public List<SkillEntry> getSkills() {

        List<SkillEntry> list = new ArrayList<SkillEntry>();
        for (Attributes a : this.getAttributes()) {
            list.addAll(a.getSkills());
        }
        return list;
    }

    @Override
    public SkillEntry getSkill(String name) {
        return RPGPlayerHelper.getPrioritySkill(this, name);
    }

    @Override
    public boolean canUseItem(String itemName, RPGComponent player) {
        boolean bool = false;
        for (Attributes a : this.getAttributes()) {
            if (a.canUseItem(itemName, this)) {
                bool = true;
            }
        }
        return bool;
    }

    @Override
    public List<ItemEntry> getItems() {
        List<ItemEntry> list = new ArrayList<ItemEntry>();
        for (Attributes a : this.getAttributes()) {
            list.addAll(a.getItems());
        }
        return list;
    }

    @Override
    public ItemEntry getItem(String name) {
        return RPGPlayerHelper.getPriorityItem(this, name);
    }

    @Override
    public List<String> getItemNames() {
        List<String> list = new ArrayList<String>();
        for (Attributes a : this.getAttributes()) {
            list.addAll(a.getItemNames());
        }
        return list;
    }

    @Override
    public boolean isItemEntryBanned(String name) {
        boolean bool = false;
        for (Attributes a : this.getAttributes()) {
            if (a.isItemEntryBanned(name)) {
                bool = true;
            }
        }
        return bool;
    }

    @Override
    public void load(DataAccess a) {
        this.access = (RPGPlayerAccess)a;
        player = (Player)super.getOwner();
        mana = new Mana(access, this);
        stamina = new Stamina(access, this);
        experience = new Experience(access, this);
        SkillPointAccess sAccess = new SkillPointAccess();
        try {
            Database.get().load(sAccess, new String[] {"playerName"}, new Object[] {player.getName()});
        } catch (DatabaseReadException ex) {
            Warriors.logger().severe(String.format("Error getting SkillPoint Data for player %s", player.getName()));
        }
        points = new SkillPoints(sAccess, this);
        for (String name : access.classes) {
            RPGClass rpg = Warriors.getRPGClassLoader().get(name);
            if (rpg != null) {
                classes.add(rpg);
            }
        }
        Race r = Warriors.getRaceLoader().get(access.race);
        if (r != null) {
            race = r;
        }
    }

    @Override
    public void save() {
        try {
            this.access.health = this.health.getHealth();
            this.access.mana = this.mana.getMana();
            this.access.stamina = this.stamina.getStamina();
            this.access.xp = this.experience.getXP();
            this.access.classes = this.getRPGClassNames();
            this.access.race = this.getRace().getName();
            Database.get().update(access, new String[] {"health", "mana", "stamina", "xp", "classes", "race"}, new Object[] {this.access.health, this.access.mana, this.access.stamina, this.access.xp, this.access.classes, this.access.race});
        } catch (DatabaseWriteException ex) {
            Warriors.logger().severe(String.format("Error setting Player Data for player %s", player.getName()));
        }
    }
}
