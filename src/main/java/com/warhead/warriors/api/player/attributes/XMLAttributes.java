/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

import com.warhead.warriors.api.item.ItemEntry;
import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.api.achievements.Requirements;
import com.warhead.warriors.api.skills.SkillEntry;
import com.warhead.warriors.exceptions.AttributeConfigException;
import com.warhead.warriors.xstream.XAttributes;
import com.warhead.warriors.xstream.XItem;
import com.warhead.warriors.xstream.XSkill;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somners
 */
public abstract class XMLAttributes implements Attributes {

    private List<SkillEntry> skills = new ArrayList<SkillEntry>();
    private List<ItemEntry> items = new ArrayList<ItemEntry>();
    private List<String> bannedItems = new ArrayList<String>();
    private String name;

    public XMLAttributes(XAttributes attributes) throws AttributeConfigException {
        name = attributes.name;
        if (attributes.skills != null) {
            for (XSkill x : attributes.skills) {
                int level = x.requirements.level == null ? 0 : x.requirements.level.intValue();
                Requirements r = new Requirements(level, x.requirements.achievements, null, null);
                int priority = x.priority == null ? 0 : x.priority;
                int baseCast = x.baseCastTime == null ? 0 : x.baseCastTime;
                int minCast = x.minCastTime == null ? 0 : x.minCastTime;
                int castI = x.castTimeIncrement == null ? 1 : x.castTimeIncrement;
                int baseCool = x.baseCoolDown == null ? 0 : x.baseCoolDown;
                int minCool = x.minCoolDown == null ? 0 : x.minCoolDown;
                int coolI = x.coolDownIncrement == null ? 1 : x.coolDownIncrement;
                int mana = x.manaCost == null ? 0 : x.manaCost;
                int stamina = x.staminaCost == null ? 0 : x.staminaCost;
                SkillEntry skill = new SkillEntry(x.name, priority, baseCast, minCast, castI, baseCool, minCool, coolI, mana, stamina, r);
                skills.add(skill);
            }
        }
        if (attributes.items != null) {
            for (XItem x : attributes.items) {
                int level = x.requirements.level == null ? 0 : x.requirements.level.intValue();
                Requirements r = new Requirements(level, x.requirements.achievements, null, null);
                int priority = x.priority == null ? 0 : x.priority;
                int baseAttackSpeed = x.baseAttackSpeed == null ? 1 : x.baseAttackSpeed;
                int minAttackSpeed = x.minAttackSpeed == null ? 1 : x.minAttackSpeed;
                int baseDamage = x.baseDamage == null ? 1 : x.baseDamage;
                int maxDamage = x.maxDamage == null ? 1 : x.maxDamage;
                int attackSpeedIncrement = x.attackSpeedIncrement == null ? 1 : x.attackSpeedIncrement;
                int damageIncrement = x.damageIncrement == null ? 1 : x.damageIncrement;
                ItemEntry item = Warriors.getFactory().getItem(x.name, priority, baseAttackSpeed, minAttackSpeed, attackSpeedIncrement, baseDamage, maxDamage, damageIncrement, r);
                if (item == null) {
                    throw new AttributeConfigException(String.format("Item '%s' does not Exist within game", x.name));
                }
                items.add(item);
            }
        }
        if (attributes.bannedItems != null) {
            this.bannedItems = attributes.bannedItems;
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean canUseSkill(String skillName, RPGComponent player) {
        SkillEntry skill = null;
        for (SkillEntry s : this.skills) {
            if (s.getName().equalsIgnoreCase(skillName)) {
                skill = s;
                break;
            }
        }
        if (skill == null) {
            return false;
        }
        return skill.getRequirements().meetsRequirements(player);
    }

    @Override
    public List<String> getSkillNames() {
        List<String> names = new ArrayList<String>();
        for (SkillEntry s : skills) {
            names.add(s.getName());
        }
        return names;
    }

    @Override
    public List<SkillEntry> getSkills() {
        return skills;
    }

    @Override
    public SkillEntry getSkill(String name) {
        SkillEntry skill = null;
        for (SkillEntry s : this.skills) {
            if (s.getName().equalsIgnoreCase(name)) {
                skill = s;
                break;
            }
        }
        return skill;
    }

    @Override
    public boolean canUseItem(String itemName, RPGComponent player) {
        ItemEntry item = null;
        for (ItemEntry s : this.items) {
            if (s.getName().equalsIgnoreCase(itemName)) {
                item = s;
                break;
            }
        }
        if (item == null) {
            return false;
        }
        return item.getRequirements().meetsRequirements(player);
    }

    @Override
    public List<ItemEntry> getItems() {
        return items;
    }

    @Override
    public ItemEntry getItem(String name) {
        ItemEntry item = null;
        for (ItemEntry s : this.items) {
            if (s.getName().equalsIgnoreCase(name)) {
                item = s;
                break;
            }
        }
        return item;
    }

    public List<String> getItemNames() {
        List<String> names = new ArrayList<String>();
        for (ItemEntry s : items) {
            names.add(s.getName());
        }
        return names;
    }

    @Override
    public boolean isItemEntryBanned(String name) {
        boolean flag = false;
        for (String s : this.bannedItems) {
            if(s.equalsIgnoreCase(name)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

}
