/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

import com.warhead.warriors.api.item.ItemEntry;
import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.attributes.data.SkillPointAccess;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.canarymod.database.Database;
import net.canarymod.database.exceptions.DatabaseWriteException;

/**
 *
 * @author Somners
 */
public class SkillPoints {

    private SkillPointAccess access;
    private RPGComponent player;

    public SkillPoints(SkillPointAccess access, RPGComponent player) {
        this.access = access;
        this.player = player;
    }

    /**
     * Gets Skill Points for the given { @link SkillPointType }.
     * @param type { @link SkillPointType } to get.
     * @return the value.
     */
    public int getPoints(SkillPointType type) {
        for (Field field : access.getClass().getFields()) {
            if (field.getName().equals(type.getInternalName())) {
                try {
                    return field.getInt(access);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return -999;
    }

    /**
     * Sets the value for the given { @link SkillPointType }.
     * @param type the { @link SkillPointType } to set.
     * @param toSet value to set to.
     */
    public void setPoints(SkillPointType type, int toSet) {
        for (Field field : access.getClass().getFields()) {
            if (field.getName().equals(type.getInternalName())) {
                try {
                    field.setInt(access, toSet);
                    Database.get().update(access, new String[]{type.getInternalName()}, new Object[]{field.getInt(access)});
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DatabaseWriteException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Add points to the given { @link SkillPointType }.
     * @param type The { @link SkillPointType } to add to.
     * @param toAdd Value to add to the existing value.
     */
    public void addPoints(SkillPointType type, int toAdd) {
        for (Field field : access.getClass().getFields()) {
            if (field.getName().equals(type.getInternalName())) {
                try {
                    field.setInt(access, field.getInt(access) + toAdd);
                    Database.get().update(access, new String[]{type.getInternalName()}, new Object[]{field.getInt(access)});
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DatabaseWriteException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Removes points to the given { @link SkillPointType }.
     * @param type The { @link SkillPointType } to remove from.
     * @param toRemove Value to remove to the existing value.
     */
    public void removePoints(SkillPointType type, int toRemove) {
        for (Field field : access.getClass().getFields()) {
            if (field.getName().equals(type.getInternalName())) {
                try {
                    field.setInt(access, field.getInt(access) - toRemove);
                    Database.get().update(access, new String[]{type.getInternalName()}, new Object[]{field.getInt(access)});
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                } catch (DatabaseWriteException ex) {
                    Logger.getLogger(SkillPoints.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public boolean isSkillPointsMaxed(SkillPointType type) {
        if (type.equals(SkillPointType.AREA_OF_EFFECT)) {

        } else if (type.equals(SkillPointType.ATTACK_DAMAGE)) {
            int diff = 0;
            for (ItemEntry item : player.getItems()) {
                int i = ((item.getMaxDamage() - item.getBaseDamage())/item.getDamageIncrement());
                if (i > diff) {
                    diff = i;
                }
            }
            return diff >= this.getPoints(SkillPointType.ATTACK_DAMAGE);
        } else if (type.equals(SkillPointType.ATTACK_SPEED)) {

        } else if (type.equals(SkillPointType.CAST_SPEED)) {

        } else if (type.equals(SkillPointType.HEALTH)) {
            return player.getHealth().isSkillPointMaxed();
        } else if (type.equals(SkillPointType.ITEM)) {

        } else if (type.equals(SkillPointType.MANA)) {
            return player.getStamina().isSkillPointMaxed();
        } else if (type.equals(SkillPointType.STAMINA)) {
            return player.getStamina().isSkillPointMaxed();
        } else if (type.equals(SkillPointType.VISUAL)) {

        }
        return true;
    }
}
