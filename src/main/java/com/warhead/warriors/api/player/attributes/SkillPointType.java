/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

import com.warhead.warriors.api.Warriors;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Somners
 */
public class SkillPointType {

    public static final SkillPointType HEALTH = new SkillPointType(0 , new String[]{"Health"}, "health");
    public static final SkillPointType ITEM = new SkillPointType(1 ,new String[]{"Item"}, "item");
    public static final SkillPointType MANA = new SkillPointType(2 ,new String[]{"Mana"}, "mana");
    public static final SkillPointType STAMINA = new SkillPointType(3 ,new String[]{"Stamina"}, "stamina");
    public static final SkillPointType ATTACK_SPEED = new SkillPointType(4 ,new String[]{"Attack Speed"}, "attackSpeed");
    public static final SkillPointType ATTACK_DAMAGE = new SkillPointType(5 ,new String[]{"Attack Damage"}, "skillPointType");
    public static final SkillPointType CAST_SPEED = new SkillPointType(6 ,new String[]{"Cast Speed"}, "castSpeed");
    public static final SkillPointType VISUAL = new SkillPointType(7 ,new String[]{"Visual"}, "visual");
    public static final SkillPointType AREA_OF_EFFECT = new SkillPointType(8 ,new String[]{"Area Of Effect", "AOE"}, "areaOfEffect");
    public static final SkillPointType COOLDOWN = new SkillPointType(8 ,new String[]{"Cooldown", "CoolDown"}, "coolDown");
    public static final SkillPointType UNALLOCATED = new SkillPointType(9 ,new String[]{"Unallocated"}, "unallocated");

    private int id;
    private String[] aliases;
    private String internalName;
    private static List<SkillPointType> types = new ArrayList<SkillPointType>();

    public SkillPointType(int id, String[] aliases, String internalName) {
        this.id = id;
        this.aliases = aliases;
        this.internalName = internalName;
    }

    public String getName() {
        return aliases[0];
    }

    public String[] getAliases() {
        return aliases;
    }

    public int getId() {
        return id;
    }

    public String getInternalName() {
        return internalName;
    }

    public static SkillPointType fromId(int id) {
        for (SkillPointType type: types) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

    public static SkillPointType fromClass(String name) {
        for (SkillPointType type: types) {
            for (String s: type.getAliases()) {
                if (s.equalsIgnoreCase(name)) {
                    return type;
                }
            }
        }
        return null;
    }

    static{
        if (ITEM == null) { // Temporary Check to make sure this works.
            Warriors.get().logger().log(Level.SEVERE, "Static faillure in class SkillPointType line 79.");
        }
        types.add(ITEM);
        types.add(HEALTH);
        types.add(MANA);
        types.add(STAMINA);
        types.add(ATTACK_SPEED);
        types.add(ATTACK_DAMAGE);
        types.add(CAST_SPEED);
        types.add(VISUAL);
        types.add(AREA_OF_EFFECT);
        types.add(UNALLOCATED);
        types.add(COOLDOWN);
    }
}
