/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

import com.warhead.warriors.api.item.ItemEntry;
import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.skills.SkillEntry;
import java.util.List;

/**
 * An interface for shared Attributes between Races and Classes.
 *
 * @author Somners
 */
public interface Attributes {

    /**
     * Gets the name for this RPG Class or Race.
     *
     * @return The name
     */
    public String getName();

    /**
     * Checks whether or not the player can use the following skill
     * @param skillName Name of the skill to check.
     * @param player The player to check for.
     * @return true if the user can use the skill, false otherwise.
     */
    public boolean canUseSkill(String skillName, RPGComponent player);

    /**
     * Gets a list of all the skills this player can use.
     * @return A list of skills.
     */
    public List<String> getSkillNames();

    /**
     * Gets a list of skill objects this player can use.
     * @return A list of skills.
     */
    public List<SkillEntry> getSkills();

    /**
     * Gets the { @link SkillEntry } by the given name
     * @param name Name of the { @link SkillEntry } to get.
     * @return The { @link SkillEntry }
     */
    public SkillEntry getSkill(String name);

    /**
     * Checks whether or not the player can use the following { @Link Item }
     * @param itemName Name of the { @Link Item } to check.
     * @param player The player to check for.
     * @return true if the user can use the { @Link Item }, false otherwise.
     */
    public boolean canUseItem(String itemName, RPGComponent player);

    /**
     * Gets a list of { @link ItemEntry }'s that is Attribute managed.
     * @return A list of items.
     */
    public List<ItemEntry> getItems();

    /**
     * Gets an { @link ItemEntry } that is Attribute managed.
     * @param name The name of the item to get.
     * @return The { @link ItemEntry } or null if the item isn't Attribute managed.
     */
    public ItemEntry getItem(String name);

    /**
     * Gets a list of all the items this player can use.
     * @return A list of items.
     */
    public List<String> getItemNames();

    /**
     * Checks if the { @link ItemEntry } is banned, meaning the user cannot use this item.
     * @param name The name of the { @link ItemEntry } to check.
     * @return True if the user cannot use the item, false if they can.
     */
    public boolean isItemEntryBanned(String name);

}
