/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.player.attributes;

/**
 * This is an interface for Attributes that can be maxed out by skill points so
 * that users don't waste their skill points.
 * @author Somners
 */
public interface SkillPointMaxable {

    /**
     * Checks if skill points are maxed for this attribute.
     * @return true if maxed out, false if can still be applied.
     */
    public boolean isSkillPointMaxed();
}
