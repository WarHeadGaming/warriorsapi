/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.rpgclass;

import com.warhead.warriors.api.achievements.Requirements;
import com.warhead.warriors.api.player.attributes.XMLAttributes;
import com.warhead.warriors.exceptions.AttributeConfigException;
import com.warhead.warriors.xstream.XClass;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somners
 */
public class XMLRPGClass extends XMLAttributes implements RPGClass {

    private Requirements requirements = null;
    private List<String> bannedRaces = new ArrayList<String>();
    private List<String> bannedClasses = new ArrayList<String>();



    public XMLRPGClass(XClass xclass) throws AttributeConfigException {
        super(xclass);
        int level = xclass.requirements.level == null ? 0 : xclass.requirements.level;
        requirements = new Requirements(level, xclass.requirements.achievements, xclass.requirements.race, xclass.requirements.rpgClass);
        if (xclass.bannedRaces != null) {
            this.bannedRaces = xclass.bannedRaces;
        }
    }

    @Override
    public List<String> getBannedRaces() {
        return this.bannedRaces;
    }

    @Override
    public Requirements getRequirements() {
        return requirements;
    }

    @Override
    public List<String> getBannedRPGClasses() {
        throw new UnsupportedOperationException("Method 'getBannedRPGClasses' in class 'XMLRPGClass' is not supported yet.");
    }

}
