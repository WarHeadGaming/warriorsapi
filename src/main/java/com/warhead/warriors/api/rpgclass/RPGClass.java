/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.rpgclass;

import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.achievements.Requirements;
import com.warhead.warriors.api.player.attributes.Attributes;
import java.util.List;

/**
 *
 * @author Somners
 */
public interface RPGClass extends Attributes, Named {

    /**
     * Gets a list of banned { @link Race } names, meaning this RPGClass cannot
     * use them.
     * @return A list of { @link Race } names.
     */
    public List<String> getBannedRaces();

    /**
     * Gets a list of banned { @link RPGClass } names, meaning this RPGClass cannot
     * use them.
     * @return A list of { @link RPGClass } names.
     */
    public List<String> getBannedRPGClasses();

    /**
     * Gets the Requirements for a player to use this Class/Race.
     * @return A list of Achievements
     */
    public Requirements getRequirements();
}
