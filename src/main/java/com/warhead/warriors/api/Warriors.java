/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api;

import com.warhead.warriors.api.loaders.AchievementLoader;
import com.warhead.warriors.api.loaders.RPGClassLoader;
import com.warhead.warriors.api.loaders.RaceLoader;
import com.warhead.warriors.api.loaders.SkillLoader;
import com.warhead.warriors.api.skills.CastTimer;
import com.warhead.warriors.api.skills.CoolDown;
import com.warhead.warriors.commandsys.CommandManager;
import com.warhead.warriors.commandsys.selection.CommandSelection;
import org.spout.api.plugin.Plugin;
import org.spout.api.plugin.PluginLogger;
import org.spout.vanilla.ChatStyle;

/**
 *
 * @author Somners
 */
public abstract class Warriors extends Plugin {

    private static Warriors instance;
    private static PluginLogger pLogger;
    private static String messageHeader = ChatStyle.BLACK + "[" + ChatStyle.GOLD + "Warriors" + ChatStyle.BLACK + "] " + ChatStyle.WHITE;
    private static AchievementLoader aLoader = null;
    private static SkillLoader sLoader = null;
    private static RaceLoader rLoader = null;
    private static RPGClassLoader cLoader = null;
    private static CoolDown cooldown = null;
    private static CastTimer casttimer = null;
    private static CommandSelection selection = null;

    /*
     * To be set by Implementing class
     */
    private static Factory factory = null;

    @Override
    public void onEnable() {
        instance = this;
        this.initLogger();
        cooldown = new CoolDown();
        casttimer = new CastTimer();
        aLoader = new AchievementLoader();
        aLoader.loadAll();
        sLoader = new SkillLoader();
        sLoader.loadAll();
        rLoader = new RaceLoader();
        rLoader.loadAll();
        cLoader = new RPGClassLoader();
        cLoader.loadAll();
        CommandManager.get();
        selection = new CommandSelection();


    }

    public void onDisable() {

    }

    /**
     * Gets the Singleton Warriors Instance
     * @return { @link Warriors }
     */
    public static Warriors get() {
        return instance;
    }

    private void initLogger() {
        pLogger =(PluginLogger)super.getLogger();
        StringBuilder sb = new StringBuilder();
        sb.append(ChatStyle.PURPLE.getChar()).append("[").append(ChatStyle.GOLD.getChar());
        sb.append("Warriors").append(ChatStyle.PURPLE.getChar()).append("]").append(ChatStyle.RESET.getChar());
        pLogger.setTag(sb.toString());
    }

    /**
     * Gets the { @link Warriors } custom logger.
     * @return The logger.
     */
    public static PluginLogger logger() {
        return pLogger;
    }

    /**
     * Gets the formatted 'Warriors' Chat Header for sending player messages.
     * @return Chat Header.
     */
    public static String getHeader() {
        return messageHeader;
    }

    /**
     * Gets the { @link AchievementLoader } singleton instance.
     * @return { @link AchievementLoader }
     */
    public static AchievementLoader getAchievmentLoader() {
        return aLoader;
    }

    /**
     * Gets the { @link SkillLoader } singleton instance.
     * @return { @link SkillLoader }
     */
    public static SkillLoader getSkillLoader() {
        return sLoader;
    }

    /**
     * Gets the { @link RaceLoader } singleton instance.
     * @return { @link RaceLoader }
     */
    public static RaceLoader getRaceLoader() {
        return rLoader;
    }

    /**
     * Gets the { @link RPGClassLoader } singleton instance.
     * @return { @link RPGClassLoader }
     */
    public static RPGClassLoader getRPGClassLoader() {
        return cLoader;
    }

    /**
     * Gets the factory for game specific objects.
     * @return
     */
    public static Factory getFactory() {
        return factory;
    }

    /**
     * Gets the { @link CoolDown } instance.
     * @return
     */
    public static CoolDown getCoolDown() {
        return cooldown;
    }

    /**
     * Gets the { @link CastTimer } instance.
     * @return
     */
    public static CastTimer getCastTimer() {
        return casttimer;
    }

    /**
     * Gets the Command Selection manager
     * @return { @link CommandSelection }
     */
    public static CommandSelection getCommandSelections() {
        return selection;
    }
}
