/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.item;

import com.warhead.warriors.api.achievements.Requirements;

/**
 *
 * @author Somners
 */
public abstract class ItemEntry {

    private String itemName;
    private int priority;
    private int baseAttackSpeed;
    private int minAttackSpeed;
    private int attackSpeedIncrement;
    private int baseDamage;
    private int maxDamage;
    private int damageIncrement;
    private Requirements requirements;

    public ItemEntry(String item, int priority, int baseAttackSpeed, int minAttackSpeed, int attackSpeedIncrement,
            int baseDamage, int maxDamage, int damageIncrement, Requirements requirements) {
        this.itemName = item;
        this.priority = priority;
        this.baseAttackSpeed = baseAttackSpeed;
        this.minAttackSpeed = minAttackSpeed;
        this.attackSpeedIncrement = attackSpeedIncrement;
        this.baseDamage = baseDamage;
        this.maxDamage = maxDamage;
        this.requirements = requirements;
    }

    /**
     * Gets the name of the held { @link Item }.
     * @return
     */
    public String getName() {
        return itemName;
    }

    /**
     * Gets the priority for this item over duplicate entries from other races/classes.
     * low numbers are low priority, higher numbers are higher priority.
     * @return the priority over duplicate entries from other races/classes.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Gets the base cast time for this { @link Item } in seconds
     * @return base cast time
     */
    public int getBaseAttackSpeed() {
        return baseAttackSpeed;
    }

    /**
     * Gets the min cast time for this { @link Item } in seconds
     * @return minimum cast time
     */
    public int getMinAttackSpeed() {
        return minAttackSpeed;
    }

    /**
     * Gets the multiplier to reduce the cast time by times item points applied
     * in seconds.
     * @return item point increment
     */
    public int getAttackSpeedIncrement() {
        return attackSpeedIncrement;
    }

        /**
     * Gets the { @link Achievement }'s required to use this item.
     * @return A list of { @link Achievement }'s.
     */
    public Requirements getRequirements() {
        return requirements;
    }

    /**
     * Gets the Base damage this item performs when used to attack.
     * @return The Base Damage.
     */
    public int getBaseDamage() {
        return this.baseDamage;
    }

    /**
     * Gets the Max Damage this item performs when used to attack.
     * @return The Max Damage.
     */
    public int getMaxDamage() {
        return this.maxDamage;
    }

    /**
     * Gets the amount Damage increments with each applied skill point.
     * @return The Incrementation amount.
     */
    public int getDamageIncrement() {
        return this.damageIncrement;
    }
}
