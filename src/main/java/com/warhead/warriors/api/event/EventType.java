/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.spout.api.event.EventHandler;
import org.spout.api.event.entity.*;
import org.spout.api.event.player.*;

/**
 *
 * @author Somners
 */
public class EventType {

    public static final EventType ENTITY_CHANGE_WORLD = new EventType(new EntityChangeWorldEventBridge());
    public static final EventType ENTITY_COLLIDE_BLOCK = new EventType(new EntityCollideBlockEventBridge());
    public static final EventType ENTITY_COLLIDE_ENTITY = new EventType(new EntityCollideEntityEventBridge());
    public static final EventType ENTITY_HIDDEN = new EventType(new EntityHiddenEventBridge());
    public static final EventType ENTITY_INTERACT_BLOCK = new EventType(new EntityInteractBlockEventBridge());
    public static final EventType ENTITY_INTERACT_ENTTIY = new EventType(new EntityInteractEntityEventBridge());
    public static final EventType ENTITY_SHOWN = new EventType(new EntityShownEventBridge());
    public static final EventType ENTITY_SPAWN = new EventType(new EntitySpawnEventBridge());
    public static final EventType ENTTIY_TELEPORT = new EventType(new EntityTeleportEventBridge());
    public static final EventType PLAYER_CHAT = new EventType(new PlayerChatEventBridge());
    public static final EventType PLAYER_INTERACT_BLOCK = new EventType(new PlayerInteractBlockEventBridge());
    public static final EventType PLAYER_INERTERACT_ENTITY = new EventType(new PlayerInteractEntityEventBridge());

    /**
     * A Mapping of { @link Triggerable }s to Event Types, for { @link Triggerable }s Triggering.
     */
    private static HashMap<EventType, List<Triggerable>> types = new HashMap<EventType, List<Triggerable>>();

    private EventBridge bridge;

    public EventType(EventBridge bridge) {
        this.bridge = bridge;
    }

    /**
     * Gets the { @link EventBridge } for this Event Type.
     * @return
     */
    public EventBridge getEventBridge() {
        return bridge;
    }

    public static EventType fromClass(Class<?> clazz) {
        return null;
    }

    /**
     * Registers the given triggerable with the given event to be called accordingly.
     * @param triggerable triggerable being registered.
     * @param type Event Type to register.
     */
    public static void register(Triggerable triggerable, EventType type) {
        if (!types.containsKey(type)) {
            /*
             * Here the event hasn't been registered by a triggerable befor so we also
             * need to register the Warriors Listener with spout.
             */
            List<Triggerable> list = new ArrayList<Triggerable>();
            list.add(triggerable);
            types.put(type, list);
            type.getEventBridge().register();
        } else {
            types.get(type).add(triggerable);
        }
    }


    public static class EntityChangeWorldEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityChangeWorldEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_CHANGE_WORLD;
        }
    }

    public static class EntityCollideBlockEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityCollideBlockEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_COLLIDE_BLOCK;
        }
    }

    public static class EntityCollideEntityEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityCollideEntityEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
            super.onEvent(event, types.get(this.getEventType()), event.getCollided());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_COLLIDE_ENTITY;
        }
    }

    public static class EntityHiddenEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityHiddenEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_HIDDEN;
        }
    }

    public static class EntityInteractBlockEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityInteractBlockEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_INTERACT_BLOCK;
        }
    }

    public static class EntityInteractEntityEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityInteractEntityEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
            super.onEvent(event, types.get(this.getEventType()), event.getInteracted());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_INTERACT_ENTTIY;
        }
    }

    public static class EntityShownEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityShownEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_SHOWN;
        }
    }

    public static class EntitySpawnEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntitySpawnEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTITY_SPAWN;
        }
    }

    public static class EntityTeleportEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(EntityTeleportEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.ENTTIY_TELEPORT;
        }
    }

    public static class PlayerChatEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(PlayerChatEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getPlayer());
        }

        @Override
        public EventType getEventType() {
            return EventType.PLAYER_CHAT;
        }
    }

    public static class PlayerInteractBlockEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(PlayerInteractBlockEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
        }

        @Override
        public EventType getEventType() {
            return EventType.PLAYER_INERTERACT_ENTITY;
        }
    }

    public static class PlayerInteractEntityEventBridge extends EventBridge {
        @EventHandler
        public void onEvent(PlayerInteractEntityEvent event) {
            super.onEvent(event, types.get(this.getEventType()), event.getEntity());
            super.onEvent(event, types.get(this.getEventType()), event.getInteracted());
        }

        @Override
        public EventType getEventType() {
            return EventType.PLAYER_INTERACT_BLOCK;
        }
    }

}
