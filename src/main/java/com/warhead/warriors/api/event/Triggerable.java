/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.event;

import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.event.EventType;
import org.spout.api.event.Event;

/**
 * A common interface for { @link Skill } and { @link Achievement }.
 * @author Somners
 */
public interface Triggerable extends Named {

    /**
     * Called when the event is Triggered.
     * @param event The event to call.
     */
    public void onEventTrigger(Event event);

    /**
     * The { @link EventType } that is called in onTriggerEvent(Event)
     * @return The { @link EventType }
     */
    public EventType getEventType();

}
