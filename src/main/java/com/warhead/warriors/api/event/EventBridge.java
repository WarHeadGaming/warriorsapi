/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.event;

import com.warhead.warriors.api.player.RPGComponent;
import com.warhead.warriors.api.player.RPGPlayerHelper;
import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.api.player.attributes.SkillPointType;
import com.warhead.warriors.api.skills.Skill;
import com.warhead.warriors.api.skills.SkillEntry;
import java.util.List;
import org.spout.api.Spout;
import org.spout.api.entity.Entity;
import org.spout.api.entity.Player;
import org.spout.api.event.Event;
import org.spout.api.event.Listener;

/**
 *
 * @author Somners
 */
public abstract class EventBridge implements Listener {

    /**
     * Registers the listener with Spout's Listener.
     */
    public void register() {
        Spout.getEventManager().registerEvents(this, Warriors.get());
    }

    /**
     * Gets the type of Event.
     * @return The Event Type.
     */
    public abstract EventType getEventType();

    public void onEvent(Event event, List<Triggerable> types, Entity entity) {
        /*
         * Make sure we have a Player here.
         */
        if (!(entity instanceof Player)) {
            return;
        }
        /*
         * Make sure this player has a Warriors Character
         */
        RPGComponent player = ((Player)entity).get(RPGComponent.class);
        if (player == null) {
            return;
        }
        /*
         * Cycle through registered skills.
         */
        for (Triggerable triggerable : types) {
            if (triggerable instanceof Skill) {
                /*
                 * Can Use Skill Check
                 */
                if (!player.canUseSkill(triggerable.getName(), player)) {
                    return;
                }
                SkillEntry skill = RPGPlayerHelper.getPrioritySkill(player, triggerable.getName());
                /*
                 * Cool Down and Cast Timer initial Checks
                 */
                if (Warriors.getCoolDown().isInCooldown(player.getName(), skill.getName())) {
                    player.getSpoutPlayer().sendMessage(String.format("%s %s skill is in cooldown.", Warriors.getHeader(), skill.getName()));
                    return;
                }
                if (Warriors.getCastTimer().isQueued(skill.getSkill(), event)) {
                    player.getSpoutPlayer().sendMessage(String.format("%s %s skill is being casted.", Warriors.getHeader(), skill.getName()));
                    return;
                }
                /*
                 * Initial Stamina and Mana checks
                 */
                int manaCost = skill.getManaCost();
                int staminaCost = skill.getStaminaCost();
                int coolDown = 0;
                if (skill.getManaCost() > 0) {
                    if (player.getMana().getMana() < skill.getManaCost()) {
                        player.getSpoutPlayer().sendMessage(String.format("%s You don't have enough Mana to use skill %s.", Warriors.getHeader(), skill.getName()));
                        return;
                    }
                }
                if (skill.getStaminaCost() > 0) {
                    if (player.getStamina().getStamina() < skill.getStaminaCost()) {
                        player.getSpoutPlayer().sendMessage(String.format("%s You don't have enough Stamina to use skill %s.", Warriors.getHeader(), skill.getName()));
                        return;
                    }
                }
                /*
                 * Cool Down Check
                 */
                if (skill.getBaseCoolDown() > 0) {
                    int tempCoolDown = skill.getBaseCoolDown() - (player.getSkillPoints().getPoints(SkillPointType.COOLDOWN) * skill.getCoolDownIncrement());
                    if (coolDown > 0) {
                        coolDown = tempCoolDown;
                    }
                }
                /*
                 * Cast Time Checks
                 */
                if (skill.getBaseCastTime() > 0) {
                    int castTime = skill.getBaseCastTime() - (player.getSkillPoints().getPoints(SkillPointType.CAST_SPEED) * skill.getCastTimeIncrement());
                    if (castTime > 0) {
                        player.getMana().removeMana(manaCost);
                        player.getStamina().removeStamina(staminaCost);
                        Warriors.getCastTimer().queueEvent(skill.getSkill(), event, castTime, coolDown);
                        player.getSpoutPlayer().sendMessage(String.format("%s Casting Skill:  %s", Warriors.getHeader(), skill.getName()));
                        return;
                    }
                }
                /*
                 * remove mana and stamina
                 */
                player.getMana().removeMana(manaCost);
                player.getStamina().removeStamina(staminaCost);
                Warriors.getCoolDown().addCooldown(player.getName(), skill.getName(), coolDown);
                triggerable.onEventTrigger(event);
            }
            else {
                triggerable.onEventTrigger(event);
            }

        }
    }
}
