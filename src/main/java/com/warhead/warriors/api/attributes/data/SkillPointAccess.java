/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.attributes.data;

import net.canarymod.database.Column;
import net.canarymod.database.DataAccess;

/**
 *
 * @author Somners
 */
public class SkillPointAccess extends DataAccess {

    public SkillPointAccess() {
        super("SkillPoints");
    }

    @Column(columnName = "playerName", dataType = Column.DataType.STRING, columnType = Column.ColumnType.PRIMARY)
    public String playerName;

    @Column(columnName = "health", dataType = Column.DataType.INTEGER)
    public int health;

    @Column(columnName = "item", dataType = Column.DataType.INTEGER)
    public int item;

    @Column(columnName = "mana", dataType = Column.DataType.INTEGER)
    public int mana;

    @Column(columnName = "stamina", dataType = Column.DataType.INTEGER)
    public int stamina;

    @Column(columnName = "attackSpeed", dataType = Column.DataType.INTEGER)
    public int attackSpeed;

    @Column(columnName = "skillPointType", dataType = Column.DataType.INTEGER)
    public int skillPointType;

    @Column(columnName = "castSpeed", dataType = Column.DataType.INTEGER)
    public int castSpeed;

    @Column(columnName = "visual", dataType = Column.DataType.INTEGER)
    public int visual;

    @Column(columnName = "areaOfEffect", dataType = Column.DataType.INTEGER)
    public int areaOfEffect;

    @Column(columnName = "unallocated", dataType = Column.DataType.INTEGER)
    public int unallocated;
}
