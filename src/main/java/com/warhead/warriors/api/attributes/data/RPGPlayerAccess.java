/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.attributes.data;

import java.util.List;
import net.canarymod.database.Column;
import net.canarymod.database.DataAccess;

/**
 *
 * @author Somners
 */
public class RPGPlayerAccess extends DataAccess {

    public RPGPlayerAccess() {
        super("RPGPlayer");
    }

    @Column(columnName = "playerName", dataType = Column.DataType.STRING, columnType = Column.ColumnType.PRIMARY)
    public String playerName;

    @Column(columnName = "race", dataType = Column.DataType.STRING)
    public String race;

    @Column(columnName = "classes", isList = true, dataType = Column.DataType.STRING)
    public List<String> classes;

    @Column(columnName = "health", dataType = Column.DataType.INTEGER)
    public int health;

    @Column(columnName = "mana", dataType = Column.DataType.INTEGER)
    public int mana;

    @Column(columnName = "stamina", dataType = Column.DataType.INTEGER)
    public int stamina;

    @Column(columnName = "xp", dataType = Column.DataType.INTEGER)
    public int xp;
}
