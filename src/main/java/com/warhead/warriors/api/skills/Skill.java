/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.skills;

import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.event.Triggerable;

/**
 *
 * @author Somners
 */
public interface Skill extends Triggerable, Named {

    /**
     * Gets the name of this skill.
     * @return The name.
     */
    public String getName();

    /**
     * Called when the skill is loaded by the { @link SkillLoader }
     */
    public void onLoad();
}
