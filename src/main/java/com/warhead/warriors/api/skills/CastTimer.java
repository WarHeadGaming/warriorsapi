/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.skills;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.spout.api.event.Event;

/**
 *
 * @author somners
 */

public class CastTimer {

        private static HashMap<Skill, List<Event>> map = new HashMap<Skill, List<Event>>();

        public CastTimer() {

        }

        /**
         * add a new cool-down for a skill
         * @param skill name of the player
         * @param event name of the skill
         * @param time time in seconds of cool-down
         * @param coolDown cooldown applied to skill after casting.
         * @return true = cool-down was added<br>false = cool-down already existed
         */
        public boolean queueEvent(Skill skill, Event event, int time, int coolDown){
            if(!map.containsKey(skill)){
                map.put(skill, new ArrayList());
                map.get(skill).add(event);
                Timer t = new Timer();
                t.schedule(new Task(skill, event, coolDown), time*1000);
                return true;
            }
            else {
                if(map.get(skill).contains(event)){
                    return false;
                }
                else{
                    map.get(skill).add(event);
                    Timer t = new Timer();
                    t.schedule(new Task(skill, event, coolDown), time*1000);
                    return true;
                }
            }
        }

        /**
         * add a new cool-down for a skill
         * @param skill name of the player
         * @param event name of the skill
         * @return true = skill is in cool-down <br>false = skill is not in cool-down
         */
        public boolean isQueued(Skill skill, Event event){
            if(map.containsKey(skill) && map.get(skill).contains(event)){
                return true;
            }
            return false;
        }

        private class Task extends TimerTask {
            private Skill skill;
            private Event event;
            private int coolDown = 0;

            public Task(Skill skill, Event event, int coolDown){
                this.skill = skill;
                this.event = event;
                this.coolDown = coolDown;
            }

            @Override
            public void run() {
                if(map.containsKey(skill) && map.get(skill).contains(event)){
                    skill.onEventTrigger(event);
                    map.get(skill).remove(event);
                    if(map.get(skill).isEmpty()){
                        map.remove(skill);
                    }
                }
            }
        }
}
