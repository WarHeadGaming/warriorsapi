/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.skills;

import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.api.achievements.Requirements;
import com.warhead.warriors.exceptions.AttributeConfigException;

/**
 *
 * @author Somners
 */
public class SkillEntry {

    private Skill skill;
    private int priority;
    private int baseCastTime;
    private int minCastTime;
    private int castTimeIncrement;
    private int baseCoolDown;
    private int minCoolDown;
    private int coolDownIncrement;
    private int manaCost;
    private int staminaCost;
    private Requirements requirements;

    public SkillEntry(String skill, int priority, int baseCastTime, int minCastTime, int skillPointIncrement,
            int baseCoolDown, int minCoolDown, int coolDownIncrement, int manaCost, int staminaCost,
            Requirements requirements) throws AttributeConfigException {
        Skill s = Warriors.getSkillLoader().get(skill);
        if (s == null) {
            throw new AttributeConfigException(String.format("Skill '%s' does not exist.", skill));
        }
        this.skill = s;
        this.priority = priority;
        this.baseCastTime = baseCastTime;
        this.minCastTime = minCastTime;
        this.castTimeIncrement = skillPointIncrement;
        this.requirements = requirements;
        this.baseCoolDown = baseCoolDown;
        this.minCoolDown = minCoolDown;
        this.coolDownIncrement = coolDownIncrement;
        this.manaCost = manaCost;
        this.staminaCost = staminaCost;
    }

    /**
     * Gets the name of the held { @link Skill }.
     * @return
     */
    public String getName() {
        return skill.getName();
    }

    /**
     * Gets the { @link Skill }
     * @return the { @link Skill }
     */
    public Skill getSkill() {
        return skill;
    }

    /**
     * Gets the priority for this skill over duplicate entries from other races/classes.
     * low numbers are low priority, higher numbers are higher priority.
     * @return the priority over duplicate entries from other races/classes.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Gets the base cast time for this { @link Skill } in seconds
     * @return base cast time
     */
    public int getBaseCastTime() {
        return baseCastTime;
    }

    /**
     * Gets the min cast time for this { @link Skill } in seconds
     * @return minimum cast time
     */
    public int getMinCastTime() {
        return minCastTime;
    }

    /**
     * Gets the multiplier to reduce the cast time by times skill points applied
     * in seconds.
     * @return skill point increment
     */
    public int getCastTimeIncrement() {
        return castTimeIncrement;
    }
    /**
     * Gets the base cool down for this { @link Skill } in seconds
     * @return base cool down
     */
    public int getBaseCoolDown() {
        return baseCoolDown;
    }

    /**
     * Gets the min cool down for this { @link Skill } in seconds
     * @return minimum cool down
     */
    public int getMinCoolDown() {
        return minCoolDown;
    }

    /**
     * Gets the multiplier to reduce the cool down by times skill points applied
     * in seconds.
     * @return skill point increment
     */
    public int getCoolDownIncrement() {
        return coolDownIncrement;
    }

    /**
     * Gets the requirements to use this skill
     * @return
     */
    public Requirements getRequirements() {
        return this.requirements;
    }

    /**
     * Gets the Cost of Mana to use this Skill.
     * @return
     */
    public int getManaCost() {
        return this.manaCost;
    }

    /**
     * Gets the Cost of Stamina to use this skill.
     * @return
     */
    public int getStaminaCost() {
        return this.staminaCost;
    }
}
