/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api;

/**
 *
 * @author Somners
 */
public interface Named {

    /**
     * Gets the unique name of this class for storage purposes.
     * @return
     */
    public String getName();
}
