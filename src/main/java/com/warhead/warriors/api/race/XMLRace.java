/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.race;

import com.warhead.warriors.api.achievements.Requirements;
import com.warhead.warriors.api.player.attributes.XMLAttributes;
import com.warhead.warriors.exceptions.AttributeConfigException;
import com.warhead.warriors.xstream.XRace;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somners
 */
public class XMLRace extends XMLAttributes implements Race {

    private String name = null;
    private Requirements requirements = null;
    private int maxHealth;
    private int baseHealth;
    private int healthIncrement;
    private int maxMana;
    private int baseMana;
    private int manaIncrement;
    private int maxStamina;
    private int baseStamina;
    private int staminaIncrement;
    private List<String> bannedRaces = new ArrayList<String>();

    public XMLRace(XRace xrace) throws AttributeConfigException {
        super(xrace);
        this.name = xrace.name;
        if (xrace.health == null) {
            throw new AttributeConfigException(String.format("No Health configured for race '%s'", this.name));
        }
        this.maxHealth = xrace.health.max != null ? xrace.health.max : 20;
        this.baseHealth = xrace.health.base != null ? xrace.health.base : 20;
        this.healthIncrement = xrace.health.skillPointIncrement != null ? xrace.health.skillPointIncrement : 1;
        this.maxMana = xrace.mana.max != null ? xrace.mana.max : 20;
        this.baseMana = xrace.mana.base != null ? xrace.mana.base : 20;
        this.manaIncrement = xrace.mana.skillPointIncrement != null ? xrace.mana.skillPointIncrement : 1;
        this.maxStamina = xrace.stamina.max != null ? xrace.stamina.max : 20;
        this.baseStamina = xrace.stamina.base != null ? xrace.stamina.base : 20;
        this.staminaIncrement = xrace.stamina.skillPointIncrement != null ? xrace.stamina.skillPointIncrement : 1;

    }

    @Override
    public int getMaxHealth() {
        return this.maxHealth;
    }

    @Override
    public int getBaseHealth() {
        return this.baseHealth;
    }

    @Override
    public int getHealthIncrement() {
        return this.healthIncrement;
    }

    @Override
    public int getMaxStamina() {
        return this.maxStamina;
    }

    @Override
    public int getBaseStamina() {
        return this.baseStamina;
    }

    @Override
    public int getStaminaIncrement() {
        return this.staminaIncrement;
    }

    @Override
    public int getMaxMana() {
        return this.maxMana;
    }

    @Override
    public int getBaseMana() {
        return this.baseMana;
    }

    @Override
    public int getManaIncrement() {
        return this.manaIncrement;
    }

    @Override
    public List<String> getBannedRPGClasses() {
        return this.bannedRaces;
    }


}
