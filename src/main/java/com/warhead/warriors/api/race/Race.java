/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.race;

import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.player.attributes.Attributes;
import java.util.List;

/**
 *
 * @author Somners
 */
public interface Race extends Attributes, Named {

    /**
     * Gets the Max Health for this race.
     * @return The Max Health.
     */
    public int getMaxHealth();

    /**
     * Gets the Base Health for this race.
     * @return The Base Health.
     */
    public int getBaseHealth();

    /**
     * Gets the amount Health increments with each applied skill point.
     * @return The Incrementation amount.
     */
    public int getHealthIncrement();

    /**
     * Gets the Max Stamina for this race.
     * @return The Max Stamina.
     */
    public int getMaxStamina();

    /**
     * Gets the Base Stamina for this race.
     * @return The Base Stamina.
     */
    public int getBaseStamina();

    /**
     * Gets the amount Stamina increments with each applied skill point.
     * @return The Incrementation amount.
     */
    public int getStaminaIncrement();

    /**
     * Gets the Max Mana for this race.
     * @return The Max Mana.
     */
    public int getMaxMana();

    /**
     * Gets the Base Mana for this race.
     * @return The Base Mana.
     */
    public int getBaseMana();

    /**
     * Gets the amount Mana increments with each applied skill point.
     * @return The Incrementation amount.
     */
    public int getManaIncrement();

    /**
     * Gets a list of banned { @link RPGClass } names, meaning this race cannot
     * use them.
     * @return A list of { @link RPGClass } names.
     */
    public List<String> getBannedRPGClasses();
}
