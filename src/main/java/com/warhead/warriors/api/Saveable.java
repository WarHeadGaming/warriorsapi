/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api;

import net.canarymod.database.DataAccess;

/**
 *
 * @author Somners
 */
public interface Saveable {

    /**
     * Loads a { @link DataAccess } class into this file.
     * @param access The DataAccess to load.
     */
    public void load(DataAccess access);

    /**
     * Saves the class to a { @link DataAccess } object
     */
    public void save();
}
