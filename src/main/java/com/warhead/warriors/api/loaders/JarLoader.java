/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.loaders;

import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.Warriors;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Somners
 */
public class JarLoader<T extends Named> implements ILoader<T> {

    private Loader<T> loader = null;

    public JarLoader(Loader loader) {
        this.loader = loader;
    }

    @Override
    public T load(String name) {
        T t = null;
        String[] list = loader.directory.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".jar")){
                File file = new File(loader.directory.getPath() + list[i]);
                for(String s : this.getClassNames(file)) {
                    t = this.loadUnregistered(file, s);
                    if (t != null && t.getName().equalsIgnoreCase(name) && !loader.map.containsKey(t.getName())) {
                        loader.onLoad((T)t);
                        loader.map.put(t.getName(), t);
                        Warriors.get().logger().severe(String.format(" %s '%s' has been loaded.", t.getClass().getName(), t.getName()));
                        return t;
                    } else {
                        t = null;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void load(File file){
        T t = null;

        for(String s : this.getClassNames(file)) {
            t = this.loadUnregistered(file, s);
            if (t != null && !loader.map.containsKey(t.getName())) {
                loader.onLoad((T)t);
                loader.map.put(t.getName(), t);
                Warriors.get().logger().severe(String.format(" %s '%s' has been loaded.", t.getClass().getName(), t.getName()));
            } else {
                Warriors.get().logger().severe(String.format(" Something went wrong loading %s with main class '%s'", t.getClass().getName(), s));
            }
        }
    }

    public T loadUnregistered(File file, String classPath) {
        T t = null;
        try {
            ClassLoader loader = new URLClassLoader(new URL[] {file.toURI().toURL()}, Warriors.get().getClassLoader());

            Class<?> tMain = loader.loadClass(classPath);
            t = (T) tMain.newInstance();


        } catch (InstantiationException ex) {
            Warriors.get().logger().log(Level.SEVERE, String.format("Error Loading %s: %s", t, file));
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            Warriors.get().logger().log(Level.SEVERE, String.format("Error Loading %s: %s", t, file));
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
           Warriors.get().logger().log(Level.SEVERE, String.format("Error Loading %s: %s", t, file));
            ex.printStackTrace();
        } catch (IOException ex) {
            Warriors.get().logger().log(Level.SEVERE, String.format("Error Loading %s: %s", t, file));
            ex.printStackTrace();
        }
        return t;
    }

    @Override
    public void loadAll() {
        String[] list = loader.directory.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".jar")){
                load(new File(loader.directory, list[i]));
            }
        }
    }

    public List<String> getClassNames(File jar) {
        List<String> list = new ArrayList<String>();

        try {
            URLClassLoader loader = new URLClassLoader(new URL[] {jar.toURI().toURL()}, Thread.currentThread().getContextClassLoader());
            URL achievementDescURL = loader.getResource("Warriors.properties");
            BufferedReader reader = new BufferedReader(new FileReader(new File(achievementDescURL.toURI())));
            String line = reader.readLine();
            while (line != null) {
                if (!line.trim().equalsIgnoreCase("")) {
                    list.add(line);
                }
            }
        } catch (IOException ex) {

        } catch (URISyntaxException ex) {
            Logger.getLogger(JarLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
}
