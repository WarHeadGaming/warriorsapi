/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.loaders;

import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.Warriors;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Somners
 */
public abstract class Loader<T extends Named> implements ILoader<T> {

    protected HashMap<String, T> map = new HashMap<String, T>();
    protected List<ILoader<T>> loaders = new ArrayList<ILoader<T>>();
    protected File directory = null;

     /**
     * checks if the { @link T } is loaded
     * @param name name of the { @link T }
     * @return true = the { @link T } is loaded<br>
     * false = the { @link T } is not loaded
     */
    public boolean isLoaded(String name){
        return map.containsKey(name);
    }

    /**
     * gets a List() of names of all { @link T } loaded
     * @return the List of { @link T } names
     */
    public List<String> getNames(){
        List<String> list = new ArrayList();
       String[] array = (String[])this.map.keySet().toArray();
        for(int i = 0 ; array.length < i ; i++){
            list.add(array[i]);
        }
        return list;
    }

    /**
     * Gets a List of all the { @link T }s that are loaded.
     * @return A list of { @link T }s.
     */
    public List<T> getAll() {
        List<T> list = new ArrayList();
        T[] array = (T[])this.map.values().toArray();
        for(int i = 0 ; array.length < i ; i++){
            list.add(array[i]);
        }
        return list;
    }

    /**
     * Gets a { @link T } by the given name.
     * @param name name of the { @link T } to get.
     * @return The { @link T } or null if it doesn't exist.
     */
    public T get(String name) {
        return map.get(name);
    }

    /**
     * Unloads the { @link T }.
     * @param name The name of the { @link T } to unload
     */
    public void unload(String name) {
        if(map.containsKey(name)){
            map.remove(name);
            Warriors.get().logger().severe(String.format(" Achievement '%s' has been unloaded.", name));
            return;
        }
        Warriors.get().logger().severe(String.format(" Achievement '%s' is not loaded and cannot be unloaded.", name));
    }

    /**
     * Reloads all available { @link T }
     */
    public void reloadAll() {
        map.clear();
        loadAll();
    }

    /**
     * Reloads the { @link T } by the given name.
     * @param name
     */
    public void reload(String name) {
        unload(name);
        load(name);
    }

    /**
     * Called when the loader loads an { @link T }
     * @param t the { @link T } that was loaded.
     */
    protected abstract void onLoad(T t);

    @Override
    public T load(String name) {
        T t = null;
        for (ILoader<T> l : loaders) {
            if (t == null) {
                t = l.load(name);
            }
        }
        return t;
    }

    @Override
    public void load(File file) {
        for (ILoader<T> l : loaders) {
            l.load(file);
        }
    }

    @Override
    public void loadAll() {
        for (ILoader<T> l : loaders) {
            l.loadAll();
        }
    }
}