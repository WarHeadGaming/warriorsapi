/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.loaders;

import com.thoughtworks.xstream.XStream;
import com.warhead.warriors.api.Named;
import com.warhead.warriors.api.Warriors;
import com.warhead.warriors.api.race.XMLRace;
import com.warhead.warriors.api.rpgclass.XMLRPGClass;
import com.warhead.warriors.exceptions.AttributeConfigException;
import com.warhead.warriors.xstream.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Somners
 */
public class XMLLoader<T extends Named> implements ILoader<T> {

    private Loader<T> loader = null;
    private XStream stream = null;


    public XMLLoader(Loader loader, XStream stream) {
        this.loader = loader;
        this.stream = stream;
    }

    @Override
    public T load(String name) {
        String[] list = loader.directory.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".xml")){
                List<Named> named = this.loadUnregisteredRaces(new File(loader.directory, list[i]));
                if (named != null) {
                    for (Named n : named) {
                        if (!loader.map.containsKey(n.getName())) {
                            if (!name.equalsIgnoreCase(n.getName())) {
                                continue;
                            }
                            loader.onLoad((T)n);
                            loader.map.put(n.getName(), (T)n);
                        }
                    }
                }
            }
        }
        return loader.map.get(name);
    }

    @Override
    public void load(File file){
        List<Named> named = this.loadUnregisteredRaces(file);
        if (named != null) {
            for (Named n : named) {
                if (!loader.map.containsKey(n.getName())) {
                    loader.onLoad((T)n);
                    loader.map.put(n.getName(), (T)n);
                }
            }
        }
    }

    @Override
    public void loadAll() {
        String[] list = loader.directory.list();
        for(int i = 0 ; i < list.length ; i++){
            if(list[i].contains(".xml")){
                load(new File(loader.directory, list[i]));
            }
        }
    }

    public List<Named> loadUnregistered(File file) {
        List<Named> toRet = null;
        if (loader instanceof RaceLoader) {
            toRet = this.loadUnregisteredRaces(file);
        }
        else if (loader instanceof RPGClassLoader) {
            toRet = this.loadUnregisteredRPGClasses(file);
        }
        return toRet;
    }

    public List<Named> loadUnregisteredRaces(File file) {
            List<Named> races = new ArrayList<Named>();
            InputStream in = null;
            XRaceFile raceFile = null;
            try {
                in = file.toURI().toURL().openStream();
                raceFile = (XRaceFile) stream.fromXML(in, new XRaceFile());
            } catch (IOException ex) {
                Warriors.get().logger().warning(String.format("Error Reading from Race file: %s", file.getName()));
            } finally {
                try {
                    in.close();
                } catch (IOException ex) {
                Warriors.get().logger().warning(String.format("Error closing stream from Race file: %s", file.getName()));
                }
            }
            for(XRace x :raceFile.races) {
                XMLRace xml = null;
                try {
                    xml = new XMLRace(x);
                } catch(AttributeConfigException ex) {
                    Warriors.get().logger().severe(String.format("Invalid Race Configuration for race '%s': %s", x.name, ex.getError()));
                    continue;
                }
                races.add(xml);
            }
            return races;
    }


    public List<Named> loadUnregisteredRPGClasses(File file) {
            List<Named> classes = new ArrayList<Named>();
            InputStream in = null;
            XClassFile rpgClassFile = null;
            try {
                in = file.toURI().toURL().openStream();
                rpgClassFile = (XClassFile) stream.fromXML(in, new XClassFile());
            } catch (IOException ex) {
                Warriors.get().logger().warning(String.format("Error Reading from RPGClass file: %s", file.getName()));
            } finally {
                try {
                    in.close();
                } catch (IOException ex) {
                Warriors.get().logger().warning(String.format("Error closing stream from RPGClass file: %s", file.getName()));
                }
            }
            for(XClass x :rpgClassFile.rpgClasses) {
                XMLRPGClass xml = null;
                try {
                    xml = new XMLRPGClass(x);
                } catch(AttributeConfigException ex) {
                    Warriors.get().logger().severe(String.format("Invalid RPGClass Configuration for class '%s': %s", x.name, ex.getError()));
                    continue;
                }
                classes.add(xml);
            }
            return classes;
    }

}
