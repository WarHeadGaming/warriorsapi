/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.loaders;

import com.thoughtworks.xstream.XStream;
import com.warhead.warriors.api.race.Race;
import com.warhead.warriors.xstream.XHealth;
import com.warhead.warriors.xstream.XMana;
import com.warhead.warriors.xstream.XRace;
import com.warhead.warriors.xstream.XRaceFile;
import com.warhead.warriors.xstream.XStamina;
import java.io.File;

/**
 *
 * @author Somners
 */
public class RaceLoader extends Loader<Race> {

    public XStream stream = null;

    public RaceLoader() {
        directory = new File("plugins/Warriors/Races/");
        stream = new XStream();
        stream.alias("name", String.class);
        stream.alias("health", XHealth.class);
        stream.alias("mana", XMana.class);
        stream.alias("stamina", XStamina.class);
        stream.alias("race", XRace.class);
        stream.alias("races", XRaceFile.class);
        loaders.add(new XMLLoader<Race>(this, stream));
    }

    @Override
    protected void onLoad(Race t) {

    }

}
