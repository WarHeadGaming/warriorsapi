/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.loaders;

import com.warhead.warriors.api.achievements.Achievement;
import com.warhead.warriors.api.event.EventType;
import java.io.File;

/**
 *
 * @author Somners
 */
public class AchievementLoader extends Loader<Achievement> {

    public AchievementLoader() {
        directory = new File("plugins/Warriors/Achievements/");
        loaders.add(new JarLoader<Achievement>(this ));
    }

    @Override
    protected void onLoad(Achievement t) {
        t.onLoad();
        EventType.register(t, t.getEventType());
    }

}
