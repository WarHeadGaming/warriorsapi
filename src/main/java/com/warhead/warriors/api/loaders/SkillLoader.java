/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.loaders;

import com.warhead.warriors.api.event.EventType;
import com.warhead.warriors.api.skills.Skill;
import java.io.File;

/**
 *
 * @author Somners
 */
public class SkillLoader extends Loader<Skill> {

    public SkillLoader() {
        directory = new File("plugins/Warriors/Skills/");
        loaders.add(new JarLoader<Skill>(this ));
    }

    @Override
    protected void onLoad(Skill t) {
        t.onLoad();
        EventType.register(t, t.getEventType());
    }
}
