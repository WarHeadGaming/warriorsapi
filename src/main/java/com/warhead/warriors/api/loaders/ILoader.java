/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.api.loaders;

import com.warhead.warriors.api.Named;
import java.io.File;

/**
 *
 * @author Somners
 */
public interface ILoader<T extends Named> {

    /**
     * Attempts to load a { @link T } by this name.
     * @return A { @link T } or null if it cannot be found..
     */
    public T load(String name);

    /**
     * Loads all the { @link T } or { @link T }s in this { @link File }.
     * @param file { @link T } { @link File } to load.
     */
    public abstract void load(File file);

    /**
     * Loads all the { @link T } available to all registered loaders.
     */
    public abstract void loadAll();
}
