/*
 * Copyright (c) 2013 WarHead Gaming.
 * All rights reserved.
 *
 * This file is copyright of WarHead Gaming. It is open Source and
 * free to use. It is licensed under the two-clause BSD License.
 */
package com.warhead.warriors.commandsys;

import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Somners
 */
public class CommandRequirementsTest extends TestCase {

    public CommandRequirementsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of verify method, of class CommandRequirements.
     */
    @Test
    @Ignore
    public void testVerify() {
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of verifyTest method, of class CommandRequirements.
     */
    @Test
    public void testVerifyTest() {
        System.out.println("verifyTest");
        List<String> args = null;
        CommandRequirements instance = null;
        boolean expResult = true;
        // TODO review the generated test code and remove the default call to fail.
        instance = new CommandRequirements(new String[]{"one", "two", "three"});
        args = new ArrayList<String>();
        args.add("-one");
        args.add("derp");
        args.add("-two");
        args.add("-three");

        boolean result = instance.verifyTest(args);

        System.out.println(String.format("Returns: %s", result));

        String output = CommandHelper.getStringArg(args, "one");
        System.out.println(String.format("Arg 'one' test: %s", output));
//        fail("The test case is a prototype.");
        /**
         *
         */

        assertEquals(expResult, result);
    }
}